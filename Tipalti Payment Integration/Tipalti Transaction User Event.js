/**
 * Module Description
 * 
 * This module integrates vendor bill and vendor payment transactions with Tipalti
 * 
 * Version  Date            Author			Remarks
 * 1.00     28 Jul 2015     Mike Canniff	Initial Release
 * 1.1		29 Dec 2015		Mike Canniff	Added support for deleting / canceling a VB.
 * 1.11		16 Mar 2016		Mike Canniff	Changed IDAP reference to use the Tipaltiy custom field on the vendor instead of the NS vendor name.	
 * 1.12		24 Mar 2016		Mike			Changed currency use the symbol field (ISO code) as update.
 * 1.13		30 Mar 2016		Mike			Fixed bug with null reference to ref_code.
 * 1.14		07 Apr 2016		Mike			Added check to the vendor payment to only process payments that are tied to Tipalti AP accounts.
 * 1.15		14 Jun 2016		Mike			Fixed bug with non alphanumeric data values in the refcode. 
 * 1.16		11 Aug 2016		Mike			Relaxed restriction on execution context. 
 * 1.17		12 Oct 2016		Mike			Added support for new Bills IPN. This user event code will now detect if a VB was created by the IPN process.
 * 											In this case, it will exit the function without sending the VB back to Tipalti
 * 1.18		01 Nov 2016		Mike			Added code to mark the checkbox for is paid manually to the payment record. Changed the payment transaction to use getText for
 * 											the expense category. 
 * 1.19		22 Nov 2016		Mike			Changed to use the item name instead of the item description on the item sublist. This will become the description field in XML.
 * 											Checked for Payment Hold flag - when this is checked, vendor bills will NOT be synced. 
 *
 */

/**
 * 
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function tipaltiBeforeLoad(type, form, request){
	var errmsg = 'Type = ' + type;
	nlapiLogExecution('DEBUG', 'tipaltiBeforeLoad event', errmsg);

	var currentContext = nlapiGetContext();
	var tipaltiInstallFlag = nlapiGetContext().getSetting('SCRIPT', 'custscript_installflag_tipalti');	

	if(tipaltiInstallFlag == 'T' && (type == 'create' || type == 'edit' || type == 'copy') && currentContext.getExecutionContext() == 'userinterface') {
		// errorField.setLabel('');
		var errorField = nlapiGetField('custbody_wserrorcode_tipalti');
		errorField.setDisplayType('disabled');
		nlapiSetFieldValue('custbody_wserrorcode_tipalti','');
		if (type == 'copy') {			// make sure that reference code is blank on the copy, since the beforesubmit sometimes passes in type = create
			nlapiSetFieldValue('custbody_refcode_tipalti','');		
		}
//		var externalId = form.addField('custpage_externalid_tipalti','text','');
//		externalId.setDisplayType('disabled');
//		if (type == 'create' || type == 'copy') {
//			var dt = new Date();
//			var st = dt.getTime().toString();		// get time returns number of milliseconds since 1970. Convert to string to get rid of exponential notation.
//			form.setFieldValues({custpage_externalid_tipalti:st});
//		} else {
//			form.setFieldValues({custpage_externalid_tipalti:''});
//		}
	}


} // end function


/**
 * converts a date from mm/dd/yyyy to the accepted xml format 2015-09-15T09:00:00
 * @returns stringDate
 * DateXMLFormat
 */

function convertDateToXMLFormat(input) {
	var parts = input.split('/'); //  this is splitting the date m/dd/yyyy and into an array
	var stringDate = parts[2] + '-' + ("0" + (parts[0])).slice(-2) + '-' + ("0" + parts[1]).slice(-2);	// wanted format '2015-09-15T09:00:00' need to add 1 to the month, since it starts at zero . the .slice(-2) makes it 2 digit format
	stringDate += 'T00:00:00'; 
	nlapiLogExecution('DEBUG', 'convertDateToXMLFormat', ' stringDate = ' + stringDate);
	return stringDate ; 
}

/**
 * 
 * This function invokes the Tipalti web service to push vendor bill updates to that system.
 * 
 * Vendor Bill status values only submit approved:
 * Approved - status = Open, approvalstatus = 2
 * 
 * 
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function tipaltiBeforeSubmit(type){
	var recType = nlapiGetRecordType().toLowerCase();

	var currentContext = nlapiGetContext();
	var tipaltiInstallFlag = currentContext.getSetting('SCRIPT', 'custscript_installflag_tipalti');	
	var tipaltiOneWorldFlag = currentContext.getSetting('SCRIPT', 'custscript_oneworldflag_tipalti');	
	var serviceURL = currentContext.getSetting('SCRIPT', 'custscript_wsserviceurl_tipalti');
	var approvalStatus = nlapiGetFieldValue ('approvalstatus');	
	var paymentHold = nlapiGetFieldValue ('paymenthold');	
	var ipnBill = nlapiGetFieldValue ( 'custbody_ipn_generated_bill_tipalti'); 
	//serviceURL = 'http://api.qa.payrad.com/v2/PayerFunctions.asmx';  //http://api.qa.payrad.com/v2/PayeeFunctions.asmx
	var errorField = nlapiGetField('custbody_wserrorcode_tipalti');
	var status = nlapiGetFieldValue('status');
	
	if (recType == 'vendorcredit') approvalStatus = '2';			// vendor credits do not have any approval status, so assume it is approved.
	var errmsg = 'Type = ' + type + ' rec type = ' + recType + ' ID = ' + nlapiGetRecordId() 
		+ ' exec context = ' + currentContext.getExecutionContext() 
		+ ' session object = ' + currentContext.getSessionObject('Calling Script')
		+ ' status = ' + status + ' approval = ' + approvalStatus + ' ipn bill = ' + ipnBill;
	nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);
	
	if(tipaltiInstallFlag == 'T' && (type != 'view') 
//		&& (currentContext.getExecutionContext() == 'userinterface' || currentContext.getExecutionContext() == 'scheduled') 
		&& ((approvalStatus == '2') || (status == 'VendBill:A')) 
		&& (paymentHold != 'T')
		&& (ipnBill == '' || ipnBill == null)) {

		var soap = null;
		var curDate = new Date();
		var dueDate = nlapiGetFieldValue('duedate');
		var st = Math.round(curDate.getTime() / 1000);	// number of seconds since 1970
		var tranDate = nlapiGetFieldValue('trandate');  // this is needed to be in xml
		//nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', 'tranDate before formating = ' + tranDate); //8/14/2015
		if (currentContext.getSessionObject('Calling Script') == 'IPN RESTlet') {
			errmsg = 'This Vendor Bill was created by the IPN RESTlet process. It will not be sent back to Tipalti';
			nlapiLogExecution('AUDIT', 'tipaltiBeforeSubmit event', errmsg);
			return;		
		}
		if (recType == 'vendorcredit') dueDate = tranDate;			// vendor credit does not have a due date field.
		tranDate = convertDateToXMLFormat(tranDate);
		dueDate = convertDateToXMLFormat(dueDate);

		//nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', 'tranDate AFTER formating = ' + tranDate); 
		//var processDate = nlapiGetFieldValue('custbody_process_date_vt');
		var stringDate = curDate.getFullYear() + '-' + ("0" + (curDate.getMonth() + 1)).slice(-2) + '-' + ("0" + curDate.getDate()).slice(-2);		// need to add 1 to the month, since it starts at zero . the .slice(-2) makes it 2 digit format
		stringDate += 'T00:00:00';    // for xml formatting

		var configData = nlapiLoadConfiguration('companyinformation');
		var payerName = currentContext.getSetting('SCRIPT', 'custscript_payername_tipalti');
		var canApprove = currentContext.getSetting('SCRIPT', 'custscript_canapprove_tipalti');
		var adminEmail = currentContext.getSetting('SCRIPT', 'custscript_emailcontact_tipalti');
		var apAccount = nlapiGetFieldValue('account');
		var vendor = nlapiGetFieldText('entity');
//		var payeeIdap = nlapiLookupField('vendor', vendor, 'externalid');
		var payeeIdap = nlapiLookupField('vendor', nlapiGetFieldValue('entity'), 'custentity_payeeid_tipalti');
		var privateKey = currentContext.getSetting('SCRIPT', 'custscript_privatekey_tipalti'); 		//The secret key received from Tipalti QA account value
		var EAT = '';
		var soap = '';
		var externalId = '';

		if (nlapiLookupField('account', apAccount, 'custrecord_apacct_tipalti') == 'F') {
			errmsg = 'The current AP Account for this Vendor Bill is not allowed for Tipalti';
			nlapiLogExecution('DEBUG', 'tipaltiPaymentBeforeSubmit event', errmsg);
			return;
		}
		errmsg = 'payer = ' + payerName + ' vendor = ' + nlapiGetFieldValue('entity') + ' ' + vendor + ' payee IDAP = ' + payeeIdap + ' key = ' + privateKey;
		nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);
//		if (type == 'create' || type == 'copy') {
//			// var externalId = vendor + ' ' + nlapiDateToString(curDate, 'datetime');			// add a time stamp to make unique
//			externalId = st + nlapiGetFieldText('entity');			// add a time stamp to make unique. The timestamp needs to be first, since the refcode has to be limited to 16 chars
//			externalId = externalId.replace(/ /g,"").substring(0,16);				// get rid of blanks in the string. The refcode appears not to allow blanks
//
//			//payeeIdap = externalId;
//			errmsg = 'new external ID = ' + externalId;
//			nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);
////			nlapiSetFieldValue('custpage_externalid_tipalti', externalId);
//			nlapiSetFieldValue('custbody_refcode_tipalti', externalId);
//		} else {
////			nlapiSetFieldValue('custpage_externalid_tipalti', '');
////			externalId = nlapiGetFieldValue('externalid').substring(0,16);
//			if (nlapiGetFieldValue('custbody_refcode_tipalti') != null && nlapiGetFieldValue('custbody_refcode_tipalti') != '') {
//				externalId = nlapiGetFieldValue('custbody_refcode_tipalti').substring(0,16);
//			}
//		}
		
		if (type == 'copy' || nlapiGetFieldValue('custbody_refcode_tipalti') == null || nlapiGetFieldValue('custbody_refcode_tipalti') == '') {
			externalId = st + nlapiGetFieldText('entity');			// add a time stamp to make unique. The timestamp needs to be first, since the refcode has to be limited to 16 chars
			externalId = externalId.replace(/[^A-Za-z0-9]+/g,'').substring(0,16);	// get rid of all NON alpha numeric data the string. The refcode appears not to allow these	
		//	externalId = externalId.replace(/ /g,'').substring(0,16);				
			errmsg = 'New external ID = ' + externalId;
			nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);
			nlapiSetFieldValue('custbody_refcode_tipalti', externalId);
		} else {
			externalId = nlapiGetFieldValue('custbody_refcode_tipalti').substring(0,16);		
		}

		nlapiSetFieldValue('custbody_wserrorcode_tipalti','');			// blank out any previous errors
		errmsg = 'payer = ' + payerName + ' payee = ' + payeeIdap + ' time stamp = ' + st + ' key = ' + privateKey + ' date timestamp = ' + curDate.getTime();
		nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);
	//	var eKey = payerName + payeeIdap + st + EAT;			// this is the combined key that will be encrypted.
		var eKey = payerName + st + EAT;			// this is the combined key that will be encrypted.
		eKey = Utf8.encode(eKey);
		errmsg = 'UTF key = ' + eKey;
		nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);
		// var authString = SHA256(eKey);

		var authString = CryptoJS.HmacSHA256(eKey, privateKey);  //encription 
		authString = CryptoJS.enc.Hex.stringify(authString);
		var currency = nlapiLookupField('currency', nlapiGetFieldValue('currency'), 'symbol' );

		soap = '<?xml version="1.0" encoding="UTF-8"?>\n';
		soap += '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema�instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">';
		soap += '\t<soap:Body>';
		if (type == 'delete') {				// This is a different type of transaction, so it requires different authentication parms.
			serviceURL += 'PayeeFunctions.asmx';
			EAT = externalId;
			eKey = payerName + st + EAT;
			authString = CryptoJS.HmacSHA256(eKey, privateKey);  //incription 
			authString = CryptoJS.enc.Hex.stringify(authString);
			errmsg = 'url = ' + serviceURL + ' complete key = ' + eKey + ' auth = ' + authString;
			nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);//		soap += '\t\t<CreateOrUpdateInvoices xmlns="http://Tipalti.org/">';
			soap += '\t\t<CancelInvoice xmlns="http://Tipalti.org/">';		
			soap += '\t\t\t<payerName>' + payerName + '</payerName>';
			soap += '\t\t\t<timestamp>' + st + '</timestamp>';  // type double
			soap += '\t\t\t<key>' + authString + '</key>'; // leave alone this section 
			soap += '\t\t\t<invoiceRefCode>' + externalId + '</invoiceRefCode>';
			soap += '\t\t</CancelInvoice>';
			
		} else {		
			serviceURL += 'PayerFunctions.asmx';
			errmsg = 'url = ' + serviceURL + ' complete key = ' + eKey + ' auth = ' + authString;
			nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);//		soap += '\t\t<CreateOrUpdateInvoices xmlns="http://Tipalti.org/">';
			soap += '\t\t<CreateOrUpdateInvoices xmlns="http://Tipalti.org/">';		
			soap += '\t\t\t<payerName>' + payerName + '</payerName>';
			soap += '\t\t\t<timestamp>' + st + '</timestamp>';  // type double
			soap += '\t\t\t<key>' + authString + '</key>'; // leave alone this section 
			soap += '\t\t\t<invoices>';
			soap += '\t\t\t\t<TipaltiInvoiceItem>';
		//	soap += '\t\t\t\t\t<Idap>' + externalId + '</Idap>'; 	// vendor? payeeIdap = 'MLC Vendor' 	change the i to be cap
			soap += '\t\t\t\t\t<Idap>' + payeeIdap + '</Idap>'; 		// 3/16/16 - Changed to use the custom Tipalti field as returned when creating the vendor.
			//soap += '<Currency>'  + currency  + '</Currency>';
	
			soap += '\t\t\t\t\t<InvoiceRefCode>'+  externalId   +'</InvoiceRefCode>';
			soap += '\t\t\t\t\t<InvoiceDate>' + tranDate +'</InvoiceDate>';	
			soap += '\t\t\t\t\t<InvoiceDueDate>' + dueDate +'</InvoiceDueDate>';
			soap += '\t\t\t\t\t<InvoiceLines>' ;
			
			soap += TiplatiLineItemXML('expense', currency);
			soap += TiplatiLineItemXML('item', currency);
			
			soap += '\t\t\t\t\t</InvoiceLines>' ;	
			soap += '\t\t\t\t\t<Description>Invoice from Vendor ' + nlapiEscapeXML(payeeIdap) + '</Description>';		
			soap += '\t\t\t\t\t<InvoiceInternalNotes>' + nlapiEscapeXML(nlapiGetFieldValue('memo')) + '</InvoiceInternalNotes>'; // 10-31-16 changed back to using memo field
	//		if (nlapiGetFieldValue('memo') == '')
	//			soap += '\t\t\t\t\t<Description>NetSuite Vendor Bill for Tipalti</Description>';
	//		else 
	//			soap += '\t\t\t\t\t<Description>' + nlapiEscapeXML(nlapiGetFieldValue('memo')) +'</Description>';
			if (canApprove == 'T')
				soap += '\t\t\t\t\t<CanApprove>' + 'true' + '</CanApprove>';
			soap += '\t\t\t\t</TipaltiInvoiceItem>'; 		//added 8/16/2015
			soap += '\t\t\t</invoices>'; 					//added 8/16/2015
			soap += '\t\t</CreateOrUpdateInvoices>';
		}
		
		soap += '\t</soap:Body>';	   
		soap += '</soap:Envelope>'; //end

		for (var i = 0; i < (Math.floor(soap.toString().length/3000)) + 1; i++) {
			var temp = soap.toString().substring(i*3000,i*3000 + 3000);
			if (i == 0) 
				temp = 'soap = ' + temp;		
			nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', temp);		
		}


		var soapHead = new Array();
		soapHead['Content-Type'] = 'text/xml; charset=utf-8';
		var response = nlapiRequestURL(serviceURL, soap, soapHead);
		var soapText = response.getBody();
		for (var i = 0; i < (Math.floor(soapText.toString().length/3000)) + 1; i++) {
			var temp = soapText.toString().substring(i*3000,i*3000 + 3000);
			if (i == 0) 
				temp = 'Response = ' + temp;		
			nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', temp);		
		}

		if (response.getCode() != 200)	{
			//var faultNode = nlapiSelectNode(soapXML, "//*[name()='faultstring']");
			var faultText = soapText;
			errmsg = 'Tipalti Response Code = ' + response.getCode() + ' fault text = ' + faultText;
			nlapiLogExecution('ERROR', 'tipaltiBeforeSubmit event', errmsg);
			if (faultText == '' || faultText == null) faultText = 'Tipalti Web Service Return Code = ' + response.getCode();
			nlapiSetFieldValue('custbody_wserrorcode_tipalti', faultText);
			if (adminEmail != '' && adminEmail != null) {
					var subject = "Tipalti Before Submit Transaction Reported an error";
				var ccArray = new Array();
				ccArray[0] = adminEmail;
				nlapiSendEmail('-5', adminEmail, subject, faultText, ccArray);		
			}
		} else {
			var soapXML = nlapiStringToXML(soapText);
			logResponseXML(soapXML, 'soap:Body', 0);
			var errorNode = nlapiSelectNode(soapXML, "//*[name()='errorMessage']");
			var errorNodeMsg = null;
			var errorCode = nlapiSelectNode(soapXML, "//*[name()='errorCode']");
			var errorCodeMsg = null;
			// var errorMessage = errorNode.nodeValue;
			// even though the response XML has a good 200 error code, there still may be functional errors in the document
			// Need to search for nodes called errorMessage and errorCode. The actual value is saved at the first child.
			// Save the error values to the custom error field on the form.
			errmsg = 'Results ';
			if (errorNode) {
				errmsg += ' errorMessage = ' + errorNode.nodeValue;
				var childNode = errorNode.firstChild;
				if (childNode) {
					errmsg += ' value = ' + childNode.nodeValue;
					errorNodeMsg = childNode.nodeValue;
				}
			}
			if (errorCode) {
				errmsg += ' errorCode = ' + errorCode.nodeValue;
				var childNode = errorCode.firstChild;
				if (childNode) {
					errmsg += ' value = ' + childNode.nodeValue;
					errorCodeMsg = childNode.nodeValue;
				}
			}		
			nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);
			if (errorNodeMsg || errorCodeMsg) { 
				if (errorNodeMsg != 'OK' && errorCodeMsg != 'OK') {
					nlapiSetFieldValue('custbody_wserrorcode_tipalti', errorNodeMsg + ' ' + errorCodeMsg);
					if (adminEmail != '' && adminEmail != null) {
						var subject = "Tipalti Before Submit Transaction Reported an error";
					var ccArray = new Array();
					var faultText = 'Dear Admin - The following error has occurred when creating a Vendor Bill:\n\n' 
						+  errorNodeMsg + ' ' + errorCodeMsg + ' ' + ' for Vendor ' + vendor;
					ccArray[0] = adminEmail;
					nlapiSendEmail('-5', adminEmail, subject, faultText, ccArray);		
					}
					return;
				}
				else {
					nlapiSetFieldValue('custbody_wserrorcode_tipalti', 'Success');					
				}
			}
		}	


	} // end first if

	nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', ' at the end of function ');

}  //end function

function TiplatiLineItemXML(type, currency){
	var msg = '';
	var amount = null;
	var soap = '';
//	var currency = nlapiGetFieldText('currency');
	var description = '';
	var memo = '';
	nlapiLogExecution('DEBUG', 'TiplatiLineItemXML', ' type = ' + type);
	if(nlapiGetLineItemCount(type) != 0){		
		for(var j = 1; j <= nlapiGetLineItemCount(type); j++ ){
			soap += '<InvoiceLine>';
			amount =nlapiGetLineItemValue(type, 'amount', j);
			soap += '<Currency>'  + currency  + '</Currency>';
			//soap += '<Currency>'  + ''  + '</Currency>';

			soap += '<Amount>' + amount + '</Amount>';
			//soap += nlapiGetLineItemValue(type, 'amount', j);
			msg = ' type = ' + type + ' currency = ' + currency + ' Amount = ' + amount;
			if(type == 'expense'){
				nlapiLogExecution('DEBUG', 'TiplatiLineItemXML', msg );
				if(nlapiGetLineItemText(type, 'category',j) != null && nlapiGetLineItemText (type, 'category',j) != ''){ // is this even a req?
					description = nlapiEscapeXML( nlapiGetLineItemText(type, 'category', j) );
					memo = nlapiEscapeXML(nlapiGetLineItemValue(type, 'memo', j));
					msg += ' category  memo =  '+ nlapiGetLineItemValue(type, 'memo', j) + ' category is = ' + description + ' memo = ' + memo;
					nlapiLogExecution('DEBUG', 'TiplatiLineItemXML', msg );
					soap += '<Description>' + description + '</Description>' ;
			//		soap += '<InvoiceSubject>' + description + '</InvoiceSubject>' ;
					if (memo == '' || memo == null)
						soap += '<InvoiceInternalNotes></InvoiceInternalNotes>';
					else 
						soap += '<InvoiceInternalNotes>' + memo + '</InvoiceInternalNotes>';

				}
				else if (nlapiGetLineItemText (type, 'account',j) != null && nlapiGetLineItemText (type, 'account',j) != ''){ // this is a req
					description = nlapiEscapeXML( nlapiGetLineItemText(type, 'account', j) );
					memo = nlapiEscapeXML(nlapiGetLineItemValue(type, 'memo', j));
					msg += '  account  memo =  '+ nlapiGetLineItemValue(type, 'memo', j) + ' account = ' + description + ' memo = ' + memo;
					nlapiLogExecution('DEBUG', 'TiplatiLineItemXML', msg );
					soap += '<Description>' + description + '</Description>' ;
			//		soap += '<InvoiceSubject>' + description + '</InvoiceSubject>' ;		// THIS TAG WAS CAUSING SOME PROBLEMS AND NEEDED TO FILLED IN.
					if (memo == '' || memo == null)
						soap += '<InvoiceInternalNotes></InvoiceInternalNotes>';
					else 
						soap += '<InvoiceInternalNotes>' + memo + '</InvoiceInternalNotes>';

				} // end else if
			}
			else{ // for item				
//				description = nlapiEscapeXML(nlapiGetLineItemValue(type, 'description', j));			// 11-22-16 switched from using line description to the item name. 
				description = nlapiEscapeXML(nlapiGetLineItemText(type, 'item', j));
				nlapiLogExecution('DEBUG', 'TiplatiLineItemXML', ' type = ' + type + ' description = ' + description);
				soap += '<Description>' + description + '</Description>' ;
//				soap += '<InvoiceSubject>' + description + '</InvoiceSubject>' ;
			}

			
			soap += '</InvoiceLine>';

		}// end for
	} // end if
	else{
		nlapiLogExecution('DEBUG', 'TiplatiLineItemXML', 'no line items found')
	}
return soap;
} // end function

/**
 * DEPRECATED
 * 
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function tipaltiAfterSubmit(type){
	var errmsg = 'Type = ' + type;
	nlapiLogExecution('DEBUG', 'tipaltiAfterSubmit', errmsg);

	var currentContext = nlapiGetContext();
	var tipaltiInstallFlag = nlapiGetContext().getSetting('SCRIPT', 'custscript_installflag_tipalti');	

	if(tipaltiInstallFlag == 'T' && (type == 'create' || type == 'copy') && currentContext.getExecutionContext() == 'userinterface') {
		var tranId = nlapiGetFieldValue('custpage_externalid_tipalti');
		var recType = nlapiGetRecordType();
		var recId = nlapiGetRecordId();
		errmsg = 'Tipalti External ID = ' + tranId + ' old external = ' + nlapiGetFieldValue('externalid') + ' rec ID = ' + recId;
		nlapiLogExecution('DEBUG', 'tipaltiAfterSubmit', errmsg);
		nlapiSubmitField(recType, recId, 'externalid', tranId);
	}

} 

/**
 * This function takes care of manual payment made in NetSuite instead of Tipalti. Only payments with a blank reference code/externalid can be
 * paid manually. In this case, every vendor bill that has been applied needs to be
 * retransmitted back to Tipalti with a manual payment flag. Alternatively, a payment can be edited such that a payment is unapplied. In this case,
 * the vendor bill will have the XML tag missing. All of the vendor bills in the applied list are added to a search array. This array is passed to the
 * search function to limit the governance for the transaction. A single XML call is made with all of the VBs.
 * 
 * Known issue - The search may still be limited to 1000 rows of data. 
 * 
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */

function tipaltiPaymentBeforeSubmit(type){
	var currentContext = nlapiGetContext();
	var tipaltiInstallFlag = currentContext.getSetting('SCRIPT', 'custscript_installflag_tipalti');	
	var tipaltiOneWorldFlag = currentContext.getSetting('SCRIPT', 'custscript_oneworldflag_tipalti');	
	var externalId = nlapiGetFieldValue('custbody_refcode_tipalti');			// the custom field will be set on the IPN RESTlet
	nlapiLogExecution('DEBUG', 'tipaltiPaymentBeforeSubmit Event', 'input type = ' + type + ' install flag = ' + tipaltiInstallFlag + ' externalid = ' + externalId);

	if (tipaltiInstallFlag == 'T' && (type == 'edit' || type == 'create' || type == 'delete' || type == 'paybills') && (externalId == '' || externalId == null)) {
			var recId = nlapiGetRecordId();
			var recType = nlapiGetRecordType().toLowerCase();		// some of the forms will submit rectype in all caps.
			var errmsg;
			var applyType = 'vendorbill';			// assume the child record will be for vendor bills.
			var oldRecord = nlapiGetOldRecord();
			var serviceURL = currentContext.getSetting('SCRIPT', 'custscript_wsserviceurl_tipalti');
			var payerName = currentContext.getSetting('SCRIPT', 'custscript_payername_tipalti');
			var canApprove = currentContext.getSetting('SCRIPT', 'custscript_canapprove_tipalti');
			var privateKey = currentContext.getSetting('SCRIPT', 'custscript_privatekey_tipalti'); 		//The secret key received from Tipalti QA account value
			var apAccount = nlapiGetFieldValue('apacct');
			var curDate = new Date();
			var st = Math.round(curDate.getTime() / 1000);	// number of seconds since 1970
			var EAT = '';
			var eKey = payerName + st + EAT;			// this is the combined key that will be encrypted.
			eKey = Utf8.encode(eKey);
			var authString = CryptoJS.HmacSHA256(eKey, privateKey);  //incription 
			authString = CryptoJS.enc.Hex.stringify(authString);
			var soap = '';
			var vbArray = new Array();
			var applyArray = new Array();
			var adminEmail = currentContext.getSetting('SCRIPT', 'custscript_emailcontact_tipalti');
			var ctr = 0;

			if (nlapiLookupField('account', apAccount, 'custrecord_apacct_tipalti') == 'F') {
				errmsg = 'The current AP Account for this Vendor Bill is not allowed for Tipalti';
				nlapiLogExecution('DEBUG', 'tipaltiPaymentBeforeSubmit event', errmsg);
				return;
			}			
			
			serviceURL += 'PayerFunctions.asmx';
			nlapiSetFieldValue('custbody_wserrorcode_tipalti','');			// blank out any previous errors
			// lookup all currencies loaded in the system
			var curColumn = new Array();
			curColumn[0] = new nlobjSearchColumn(  'symbol' );
			var curResults = nlapiSearchRecord( 'currency', null, null, curColumn );

			errmsg = 'url = ' + serviceURL + ' complete key = ' + eKey + ' auth = ' + authString;
			nlapiLogExecution('DEBUG', 'tipaltiPaymentBeforeSubmit event', errmsg);
			for (var i = 1; i <= nlapiGetLineItemCount('apply'); i++) {
				var applyAmount = 0.0; 
				var applyValue = 0;
				
				if (oldRecord) {				// note the validation for empty fields. The APIs return null for an empty field. 
					if (nlapiGetLineItemValue('apply', 'apply', i) == 'T' && oldRecord.getLineItemValue('apply', 'apply', i) == 'F') {
						// This line is a new apply, so use the whole amount
						applyValue = 1;
						vbArray[ctr] = nlapiGetLineItemValue('apply', 'internalid', i);
						applyArray[ctr] = 1;
						ctr++;
					}
					if (nlapiGetLineItemValue('apply', 'apply', i) == 'F' && oldRecord.getLineItemValue('apply', 'apply', i) == 'T') {
						// This line has a reversed apply amount. The checkbox was unmarked.
						vbArray[ctr] = nlapiGetLineItemValue('apply', 'internalid', i);
						applyArray[ctr] = -1;
						ctr++;
					}
				} else 			// this is a new payment
					if (nlapiGetLineItemValue('apply', 'apply', i) == 'T') {
						// This line is a new apply, so use the whole amount
						applyAmount = nlapiGetLineItemValue('apply', 'amount', i) ? parseFloat(nlapiGetLineItemValue('apply', 'amount', i)) : 0.0;
						// vendor credits will only be available in a new payment. Once a payment has been saved, then this line item is removed from the apply sublist
						// The credit is then applied to a specific vendor bill in the vendor credit transaction. 
						// Vendor credits can be ignored regarding Tipalti integration
						
						if (applyAmount > 0.0) {
							vbArray[ctr] = nlapiGetLineItemValue('apply', 'internalid', i);
							applyArray[ctr] = 1;
							ctr++;
						}
					}	
			}
			
			errmsg = 'Applied VB array length = ' + vbArray.length;
			nlapiLogExecution('DEBUG', 'tipaltiPaymentBeforeSubmit eventt', errmsg);
			
			if (vbArray.length > 0) {		// apply the header level XML tags only if some vendor bills have been updated.
				soap = '<?xml version="1.0" encoding="UTF-8"?>\n';
				soap += '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema�instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">';
				soap += '\t<soap:Body>';
				soap += '\t\t<CreateOrUpdateInvoices xmlns="http://Tipalti.org/">';		
				soap += '\t\t\t<payerName>' + payerName + '</payerName>';
				soap += '\t\t\t<timestamp>' + st + '</timestamp>';  // type double
				soap += '\t\t\t<key>' + authString + '</key>'; // leave alone this section 
				soap += '\t\t\t<invoices>';				
				var vbFilters = new Array();
				vbFilters[0] = new nlobjSearchFilter( 'internalid', null, 'anyof', vbArray );

				var vbColumns = new Array();
				vbColumns[0] = new nlobjSearchColumn( 'internalid' );
				vbColumns[1] = new nlobjSearchColumn( 'mainline' );
				vbColumns[2] = new nlobjSearchColumn( 'line' );
				vbColumns[3] = new nlobjSearchColumn( 'item' );
				vbColumns[4] = new nlobjSearchColumn( 'expensecategory' );
				vbColumns[5] = new nlobjSearchColumn( 'memo' );
				vbColumns[6] = new nlobjSearchColumn( 'account' );
				vbColumns[7] = new nlobjSearchColumn( 'entity' );
				vbColumns[8] = new nlobjSearchColumn( 'amount' );
				vbColumns[9] = new nlobjSearchColumn( 'custbody_refcode_tipalti' );
				vbColumns[10] = new nlobjSearchColumn( 'trandate' );
				vbColumns[11] = new nlobjSearchColumn( 'duedate' );
				vbColumns[12] = new nlobjSearchColumn( 'currency' );
				
				var vbResults = nlapiSearchRecord( applyType, null, vbFilters, vbColumns );
				if (!vbResults){
					errmsg = 'Could NOT find Vendor Bill = ' + nlapiGetLineItemValue('apply', 'internalid', i);
					nlapiLogExecution('ERROR', 'tipaltiPaymentBeforeSubmit event', errmsg);
					return -1;
				} else {	
					vbFound = true;
					errmsg = 'VB/VC line count = ' + vbResults.length;
					nlapiLogExecution('DEBUG', 'tipaltiPaymentBeforeSubmit event', errmsg);
					var vbLiabilityAccount;
					for (var i = 0; i < vbResults.length; i++) {
						var vbResult = vbResults[ i ];
						var isPaidManually = true;
						errmsg = 'VB[' + i + '] id = ' + vbResult.getId() + ' ' + vbResult.getValue('internalid') + ' main = ' + vbResult.getValue('mainline') + ' amount = ' + vbResult.getValue('amount') 
							+ ' account = ' + vbResult.getValue('account') + ' category = ' + vbResult.getText('expensecategory') + ' item = ' + vbResult.getValue('item');
						nlapiLogExecution('DEBUG', 'tipaltiPaymentBeforeSubmit event', errmsg);
						// mainline has an asterisk, need to begin the XML tags for a new invoice.
						if (vbResult.getValue('mainline') == '*') {
							var vendor = vbResult.getValue('entity');
							var payeeIdap = nlapiLookupField('vendor', vendor, 'custentity_payeeid_tipalti');
							var externalId = vbResult.getValue('custbody_refcode_tipalti').substring(0,16);
							var tranDate = vbResult.getValue('trandate');
							var dueDate = vbResult.getValue('duedate');
//							externalId = externalId.replace(/ /g,"").substring(0,16);				// get rid of blanks in the string. The refcode appears not to allow blanks
							tranDate = convertDateToXMLFormat(tranDate);
							dueDate = convertDateToXMLFormat(dueDate);
							errmsg = 'Header entity = ' + vendor + ' externalid = ' + externalId + ' trandate = ' + tranDate + ' duedate = ' + dueDate;
							nlapiLogExecution('DEBUG', 'tipaltiPaymentBeforeSubmit event', errmsg);
							//loop through the original apply array to determine whether this VB is a new apply or a removal
							if (i > 0)	{			// this is the next VB in the sequence, need to close out the previous XML tags							
								soap += '\t\t\t\t\t</InvoiceLines>' ;
								soap += '\t\t\t\t</TipaltiInvoiceItem>';
							}
							soap += '\t\t\t\t<TipaltiInvoiceItem>';
							soap += '\t\t\t\t\t<Idap>' + payeeIdap + '</Idap>'; 		// 3/16/16 - Changed to use the custom Tipalti field as returned when creating the vendor.
							soap += '\t\t\t\t\t<InvoiceRefCode>'+  externalId   +'</InvoiceRefCode>';
							soap += '\t\t\t\t\t<InvoiceDate>' + tranDate +'</InvoiceDate>';	
							soap += '\t\t\t\t\t<InvoiceDueDate>' + dueDate +'</InvoiceDueDate>';
							soap += '\t\t\t\t\t<Description>Invoice from Vendor ' + nlapiEscapeXML(payeeIdap) + '</Description>';		
							soap += '\t\t\t\t\t<InvoiceInternalNotes>' + nlapiEscapeXML(nlapiGetFieldValue('memo')) + '</InvoiceInternalNotes>'; // 10-31-16 changed back to using memo field

							for (var j = 0; j < vbArray.length; j++) {
								if (vbArray[j] == vbResult.getId()) {
									if (applyArray[j] == 1)
										soap += '\t\t\t\t\t<IsPaidManually>' + 'true'+ '</IsPaidManually>';
									break;
								}
							}
							soap += '\t\t\t\t\t<InvoiceLines>' ;
						} else {			// process the next line
							var currency = '';
							for (var j in curResults) {
								var result = curResults[j];
								if (vbResult.getValue('currency') == result.getId()) {
									currency = result.getValue('symbol');
									break;
								}
							}
							soap += '<InvoiceLine>';
//							soap += '<Currency>'  + vbResult.getText('currency')  + '</Currency>';
							soap += '<Currency>'  + currency  + '</Currency>';
							soap += '<Amount>' + vbResult.getValue('amount') + '</Amount>';
							if (vbResult.getValue('item') != '' && vbResult.getValue('item') != null) {
								soap += '<Description>' + nlapiEscapeXML( vbResult.getText('item')) + '</Description>' ;						
							} else {
								if (vbResult.getValue('expensecategory') != '' && vbResult.getValue('expensecategory') != null)
									soap += '<Description>' + nlapiEscapeXML( vbResult.getText('expensecategory')) + '</Description>' ;
								else
									soap += '<Description>' + nlapiEscapeXML( vbResult.getText('account')) + '</Description>' ;
							}
								
							soap += '<InvoiceInternalNotes>' + nlapiEscapeXML(vbResult.getValue('memo')) + '</InvoiceInternalNotes>';
							soap += '</InvoiceLine>';

						}
					}
				}
				soap += '\t\t\t\t\t</InvoiceLines>' ;				// close out all of the open tags
				soap += '\t\t\t\t</TipaltiInvoiceItem>';
				soap += '\t\t\t</invoices>'; 					
				soap += '\t\t</CreateOrUpdateInvoices>';
				soap += '\t</soap:Body>';	   
				soap += '</soap:Envelope>'; //end
	
				for (var i = 0; i < (Math.floor(soap.toString().length/3000)) + 1; i++) {
					var temp = soap.toString().substring(i*3000,i*3000 + 3000);
					if (i == 0) 
						temp = 'soap = ' + temp;		
					nlapiLogExecution('DEBUG', 'tipaltiPaymentBeforeSubmit event', temp);		
				}
	
	
				var soapHead = new Array();
				soapHead['Content-Type'] = 'text/xml; charset=utf-8';
				var response = nlapiRequestURL(serviceURL, soap, soapHead);
				var soapText = response.getBody();
				for (var i = 0; i < (Math.floor(soapText.toString().length/3000)) + 1; i++) {
					var temp = soapText.toString().substring(i*3000,i*3000 + 3000);
					if (i == 0) 
						temp = 'Response = ' + temp;		
					nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', temp);		
				}
	
				if (response.getCode() != 200)	{
					//var faultNode = nlapiSelectNode(soapXML, "//*[name()='faultstring']");
					//var faultText = 'Dear Admin - The following error has occurred:\n\n' 
					//	+ nlapiSelectValue( faultNode, "//*[name()='faultstring']");
					var faultText = soapText;
					errmsg = 'Tipalti Response Code = ' + response.getCode() + ' fault text = ' + faultText;
					nlapiLogExecution('ERROR', 'tipaltiBeforeSubmit event', errmsg);
					if (faultText == '' || faultText == null) 
						faultText = 'Tipalti Web Service Return Code = ' + response.getCode();
					nlapiSetFieldValue('custbody_wserrorcode_tipalti', faultText);
					if (adminEmail != '' && adminEmail != null) {
						var subject = "Tipalti Before Submit Transaction Reported an error";
						var ccArray = new Array();
						ccArray[0] = adminEmail;
						nlapiSendEmail('-5', adminEmail, subject, faultText, ccArray);		
					}
				} else {
					var soapXML = nlapiStringToXML(soapText);
					logResponseXML(soapXML, 'soap:Body', 0);
					var errorNode = nlapiSelectNode(soapXML, "//*[name()='errorMessage']");
					var errorNodeMsg = null;
					var errorCode = nlapiSelectNode(soapXML, "//*[name()='errorCode']");
					var errorCodeMsg = null;
					// var errorMessage = errorNode.nodeValue;
					// even though the response XML has a good 200 error code, there still may be functional errors in the document
					// Need to search for nodes called errorMessage and errorCode. The actual value is saved at the first child.
					// Save the error values to the custom error field on the form.
					errmsg = 'Results ';
					if (errorNode) {
						errmsg += ' errorMessage = ' + errorNode.nodeValue;
						var childNode = errorNode.firstChild;
						if (childNode) {
							errmsg += ' value = ' + childNode.nodeValue;
							errorNodeMsg = childNode.nodeValue;
						}
					}
					if (errorCode) {
						errmsg += ' errorCode = ' + errorCode.nodeValue;
						var childNode = errorCode.firstChild;
						if (childNode) {
							errmsg += ' value = ' + childNode.nodeValue;
							errorCodeMsg = childNode.nodeValue;
						}
					}		
					nlapiLogExecution('DEBUG', 'tipaltiPaymentBeforeSubmit event', errmsg);
					nlapiSetFieldValue('custbody_ispaidmanually_tipalti', 'T');					
					if (errorNodeMsg || errorCodeMsg) { 
						if (errorNodeMsg != 'OK' && errorCodeMsg != 'OK') {
							nlapiSetFieldValue('custbody_wserrorcode_tipalti', errorNodeMsg + ' ' + errorCodeMsg);
							if (adminEmail != '' && adminEmail != null) {
								var subject = "Tipalti Before Submit Transaction Reported an error";
								var ccArray = new Array();
								ccArray[0] = adminEmail;
								var faultText = 'Dear Admin - The following error has occurred when creating a manual payment and updating Vendor Bills:\n\n' 
									+  errorNodeMsg + ' ' + errorCodeMsg;
								nlapiLogExecution('DEBUG', 'tipaltiPaymentBeforeSubmit event', faultText);
								nlapiSendEmail('-5', adminEmail, subject, faultText, ccArray);									
								nlapiSetFieldValue('custbody_wserrorcode_tipalti', errorNodeMsg + ' ' + errorCodeMsg);
							}
							return;
						}
						else {
							nlapiSetFieldValue('custbody_wserrorcode_tipalti', 'Success');					
						}
					}
				}			// end 200 return code if	
			}				// end apply changes if


		} // end first if

	nlapiLogExecution('DEBUG', 'tipaltiPaymentBeforeSubmit event', 'end of function');

}				// end tipaltiPaymentBeforeSubmit function


