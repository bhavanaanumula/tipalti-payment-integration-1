/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       15 Jan 2016     Keely
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function tiplatiScheduledVendorSync(type) {
	var govLimit = 200;               // let the governance wind down from 10000 to 200 for this scheduled script
	var currentContext = nlapiGetContext();
	var msg = ' script is executing ';
	var paramMsg = '';	
	//var fieldSetTo = '';
	var fieldSetTo = 'sync scheduled';
	var endDate = currentContext.getSetting('SCRIPT', 'custscript_enddate_tipalti');  // grabbing the params from NS
	var startDate= currentContext.getSetting('SCRIPT', 'custscript_startdate_tipalti');	
	var synctype = currentContext.getSetting('SCRIPT', 'custscript_synctype_tipalti');
	var syncField = '';// either ref code or payee id
	paramMsg = ' endDate = ' + endDate + ' startDate = ' + startDate + 'synctype = '+ synctype;
	nlapiLogExecution('DEBUG', ' tiplatiScheduledVendorSync ', paramMsg);
	var typeArray = new Array(); //push all in there
	if((startDate == null || startDate == '')  &&  (endDate == null || endDate == '') ) 
	{
		resultmsg = 'cannot find any parameters';
		nlapiLogExecution('ERROR', ' tiplatiScheduledVendorSync ', resultmsg);
	}else {
		var filters = new Array();
		if(synctype == 'vendor'){
			nlapiLogExecution('DEBUG', ' tiplatiScheduledVendorSync ', 'inside if statment synctype == vendor');
			filters.push ( new nlobjSearchFilter( 'datecreated', null, 'notbefore', startDate ) ); //picks up the vendor created within certain dates
			filters.push (new nlobjSearchFilter( 'datecreated', null, 'notafter', endDate ) );
			var columns = new Array();
			columns[0] = new nlobjSearchColumn( 'custentity_payeeid_tipalti' );  // this is here to grab the information later for if logic
			var searchresults = nlapiSearchRecord( 'vendor', null, filters, columns ); // go to dB find all records match. 1st type, 3rd critera, col= what info from record etc
			syncField = 'custentity_wserrorcode_tipalti';//testing

		}
		else// this is for vb
		{ //need to push VB status
			nlapiLogExecution('DEBUG', ' tiplatiScheduledVendorBillSync ', 'inside else statment synctype == vb');
			filters.push ( new nlobjSearchFilter( 'trandate', null, 'notbefore', startDate ) ); //picks up the vendor created within certain dates
			filters.push (new nlobjSearchFilter( 'trandate', null, 'notafter', endDate ) );
		//  [2] = {string} VendBill:A   == Open
		//  [3] = {string} VendBill:B
		//  [4] = {string} VendBill:C
		//  [5] = {string} VendBill:D
		//  [6] = {string} VendBill:E
		//  var salesorderSearch = nlapiLoadSearch('transaction', 'customsearch996'); //internalId of the saved search that checks for vendor status
		//  var soSearchExp = salesorderSearch.getFilterExpression(); 
			filters.push (new nlobjSearchFilter( 'status', null, 'anyof', 'VendBill:A' ) );
			var columns = new Array();
			columns[0] = new nlobjSearchColumn( 'custbody_refcode_tipalti' ); 
			var searchresults = nlapiSearchRecord( 'vendorbill', null, filters, columns ); // go to dB find all records match. 1st type, 3rd critera, col= what info from record et
			syncField = 'custbody_wserrorcode_tipalti'; // testing, hopefully right field (not entity like other)
		}

		for ( var i = 0; searchresults != null && i < searchresults.length; i++ ){ // to get the info from records
			var searchresult = searchresults[ i ];
			var fieldID = (searchresult.getValue(syncField )); // getting the field refcode or payeeid
			var vendorId = searchresult.getId();	// get the id of the specific vendor
			var recType = searchresult.getRecordType(); // get the recordtype of the specific vendor type. assuming gets vb
			msg = 'fieldID = ' + fieldID+ ' vendorId = ' + vendorId + ' recType = ' +recType;
			nlapiLogExecution('DEBUG', ' tiplatiScheduledVendorSync  ', msg);
			if(  fieldID == null || fieldID == '' ) // this makes sure that only the vendors with the blank field gets updated.
			{
				try{
					var record = nlapiLoadRecord ( recType, vendorId ); // loading the record
					if( record != null ){
						nlapiLogExecution('DEBUG',' tiplatiScheduledVendorSync ', 'vendor name = ' + record.getFieldValue('companyname') );
						record.setFieldValue(syncField ,fieldSetTo );			
						var update = nlapiSubmitRecord(record ); 		
						var internalMsg = 'initial sync in process, update complete ';
						nlapiLogExecution('DEBUG',' tiplatiScheduledVendorSync ', internalMsg);
					}

				}
				catch(e)
				{
					msg = 'failed vendor payee id update' + e;
					nlapiLogExecution('ERROR',' tiplatiScheduledVendorSync ', msg);
				}
			}
			if( currentContext.getRemainingUsage() < govLimit ) {  
				var state = nlapiYieldScript();
				if( state.status == 'FAILURE')   {
					errmsg = "Failed to yield script, exiting: Reason = " + state.reason + " / Size = " + state.size;
					nlapiLogExecution('ERROR', ' tiplatiScheduledVendorSync  ', errmsg);
					return;
				} 
				else if ( state.status == 'RESUME' ){
					errmsg = "Resuming script because of " + state.reason + ".  Size = " + state.size;
					nlapiLogExecution('AUDIT', ' tiplatiScheduledVendorSync  ', errmsg);
				}
				// state.status will never be SUCCESS because a success would imply a yield has occurred.  The equivalent response would be yield
			} // end if govLimit 

		}// end for loop
	}
	nlapiLogExecution('DEBUG',' tiplatiScheduledVendorSync ', ' at the end of the function tiplatiScheduledVendorSync ');

}

function runVendorSyncProcess(){  // this is 1st executed when the button (Sync Vendors) is clicked on. 
	var post = new Array();
	alert(' tipalti vendor sync Process has been submitted ');
	post['endDate'] = nlapiGetFieldValue ('custrecord_vendorsyncenddate_tp');
	post['startDate'] = nlapiGetFieldValue('custrecord_vendorsyncstartdate_tp');
	post['synctype'] = 'vendor';
	var resolve = nlapiResolveURL('SUITELET', 'customscript_vendorsyncsuitelet_tp', 'customdeploy_vendorsyncsuitelet_tp');   
	var request = nlapiRequestURL( resolve ,post);
}

function runVendorBillSyncProcess(){  // this is 1st executed when the button (Sync Vendors bill) is clicked on. 
	var post = new Array();
	alert(' tipalti vendor bill sync Process has been submitted ');
	post['endDate'] = nlapiGetFieldValue ('custrecord_vendorsyncenddate_tp');
	post['startDate'] = nlapiGetFieldValue('custrecord_vendorsyncstartdate_tp');
	post['synctype'] = 'vb'; // vendor bill
	var resolve = nlapiResolveURL('SUITELET', 'customscript_vendorsyncsuitelet_tp', 'customdeploy_vendorsyncsuitelet_tp');   
	var request = nlapiRequestURL( resolve ,post);
}