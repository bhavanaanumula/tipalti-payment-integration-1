/**	
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       24 Dec 2015     Keely
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function tipaltiVendorSyncSuitelet(request, response){
	//grabing the info which was passed through an array
	var currentContext = nlapiGetContext();
	var endDate= request.getParameter('endDate');
	var startDate = request.getParameter('startDate');
	var synctype = request.getParameter('synctype'); // what type the record is for the if else logic
	var params = new Array();
	var msg = ' Hello suitelet, endDate =  ' + endDate + ' startDate = ' + startDate + 'synctype = ' + synctype;
	nlapiLogExecution('DEBUG', 'tipaltiVendorSyncSuitelet ', msg); //print
	//alert(msg);
	
	params['custscript_enddate_tipalti'] = endDate;
	params['custscript_startdate_tipalti'] = startDate;
	params['custscript_synctype_tipalti'] = synctype;
	//var run = nlapiScheduleScript('customscript_vendorsyncsuitelet_tp', 'customdeploy_vendorsyncsuitelet_tp', params);
	//var run = nlapiScheduleScript('customscript_syncvendors_tp', null, params);
	
	  var resubmitCtr = 0;
	   var status = nlapiScheduleScript('customscript_syncvendors_tp', null, params);
	   errmsg = 'Initial Scheduled Status = ' + status ;
	   nlapiLogExecution('DEBUG', 'Tipalti Sync Suitelet', errmsg);
	   while (status == null) {
	    var date = new Date();
	    var curDate = null;
	    do { 
	       curDate = new Date(); 
	    }   while(curDate - date < 3000);
	    status = nlapiScheduleScript('customscript_syncvendors_tp', null, params);
	    errmsg = 'Null return code from Schedule Script. Waiting for scheduled script queues = ' + resubmitCtr + ' new status = ' + status;
	    nlapiLogExecution('DEBUG', 'Tipalti Sync Suitelet', errmsg);
	    resubmitCtr++;  
	   }
	   errmsg = 'Final Scheduled Status = ' + status + ' End function governance = ' + currentContext.getRemainingUsage();
	   nlapiLogExecution('DEBUG', 'Tipalti Sync Suitelet', errmsg);
	
	//nlapiLogExecution('DEBUG', 'tipalti Suitelet Sync ', ' run = ' + run); //print

}
