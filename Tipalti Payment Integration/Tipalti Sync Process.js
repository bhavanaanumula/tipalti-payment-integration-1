/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       02 Jun 2015     Keely Canniff
 *
 */

/**This Function 
 * Can use date, accounting period,and posting period for search parameter.
 * Update the Record with the tipalti Process date which is gotten from the current system time. 
 * This code assumes that the user will not edit the invoice after they submit it in for request. 
 * 
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function tipaltiRecordsync(type) {

	var govLimit = 200;               // let the governance wind down from 10000 to 200 for this scheduled script
	var currentContext = nlapiGetContext();
	var msg = ' script is executing ';
	var paramMsg = '';

	//var postingPeriod = currentContext.getSetting('SCRIPT', 'custscript_postingperiod_tipalti');
	var endDate = currentContext.getSetting('SCRIPT', 'custscript_enddate_tipalti');
	var startDate= currentContext.getSetting('SCRIPT', 'custscript_startdate_tipalti');
//	var AccountingPeriod =  currentContext.getSetting('SCRIPT', 'custscript_accounting_period_tipalti');
	var errorCounter = 0; // this is the counter for the for-loop. this is used to keep track of successful field updates. 
	nlapiLogExecution('DEBUG', ' tipalti Record sync ', msg  + '  posting period = ' + postingPeriod + ' endDate = ' + endDate + ' start date = ' + startDate + ' AccountingPeriod = ' + AccountingPeriod);
	//paramMsg = '  posting period = ' + postingPeriod + ' endDate = ' + endDate + ' start date = ' + startDate + ' AccountingPeriod = ' + AccountingPeriod;
	paramMsg = ' endDate = ' + endDate + ' start date = ' + startDate;
	//var searchParams = nlapiSearchRecord( 'customrecord_syncparm_tipalti', null, null, columnsForParam );  //goes to the record for the info
	var resultmsg = ' searchResult = ' ;  // for testing to see what the value is for result
	var typeArray = new Array(); //push all in there
	typeArray.push ('CustCred'); //the type of transactions.
	typeArray.push ('CustInvc');
	typeArray.push ('CashSale');
	typeArray.push ('CashRfnd');

	//if(searchParams == null){ // mod it to check that all fields are null, if so error.

	if( (postingPeriod == null || postingPeriod =='') && (startDate == null || startDate == '')  &&  (endDate == null || endDate == '') && (AccountingPeriod == 'F') ) 
	{
		resultmsg = 'cannot find any parameters';
		nlapiLogExecution('ERROR', ' tipalti Record sync ', resultmsg);
	}else {
		nlapiLogExecution('DEBUG', ' tipalti Record sync ', ' inside 1st else');
		resultmsg = ' custrecord_postingperiod_tipalti =  ' + postingPeriod 
		+ ' custrecord_startdate_tipalti = ' + startDate + ' custrecord_enddate_tipalti = ' + endDate
		+ ' custrecord_accounting_period_tipalti = ' +  AccountingPeriod;
		nlapiLogExecution('DEBUG', ' tipalti Record sync ', resultmsg);
		//resultmsg = '  ' + resultmsg + searchParam + ' ';

		msg = ' the record parameters are:  Posting Period is  = ' + postingPeriod + ' startDate is = ' + startDate + ' endDate is = ' + endDate + ' AccountingPeriod is = ' + AccountingPeriod;
		var filters = new Array();
		filters.push ( new nlobjSearchFilter( 'mainline', null, 'is', 'T' ) );
		filters.push ( new nlobjSearchFilter( 'type', null, 'anyof', typeArray ) );
		//	filters.push ( new nlobjSearchFilter( 'isinactive',null, 'is', 'F' ) );


		if( startDate != null && (startDate != '' ) ){
			filters.push ( new nlobjSearchFilter( 'datecreated', null, 'notbefore', startDate ) ); //picks up the vendor created within certain dates
			filters.push (new nlobjSearchFilter( 'datecreated', null, 'notafter', endDate ) );
		}

		if(AccountingPeriod == 'T'){

			var lowIndex = -1;			
			var curdate = new Date();
			var stringDate = nlapiDateToString(curdate);
			var acctFilters = new Array();
			acctFilters[0] = new nlobjSearchFilter( 'startdate', null, 'onorbefore', curdate);
			acctFilters[1] = new nlobjSearchFilter( 'enddate', null, 'onorafter', curdate);
			var acctColumns = new Array();
			acctColumns[0] = new nlobjSearchColumn( 'periodname' );
			acctColumns[1] = new nlobjSearchColumn( 'startdate' );
			acctColumns[2] = new nlobjSearchColumn( 'enddate' );
			//var acctResults = nlapiSearchRecord('accountingperiod', null, acctFilters , acctColumns);


			var lowestDifference = 0;
			var currentDifference = 0;
			for (var kk = 0; acctResults != null && kk < acctResults.length; kk++) { 
				acctResult = acctResults[kk];
				if(kk == 0){ // for the first iteration.
					lowestDifference = nlapiStringToDate(acctResult.getValue('enddate')) - nlapiStringToDate(acctResult.getValue('startdate'));
					msg = '  '+ lowestDifference ; 
				}else{ // rest of the for loop.
					currentDifference = nlapiStringToDate(acctResult.getValue('enddate')) - nlapiStringToDate(acctResult.getValue('startdate'));
					msg = '  '+ currentDifference ; 
				}

				if (kk > 0) {
					if (currentDifference < lowestDifference) {
						lowestDifference = currentDifference;
						lowIndex = kk;
					} 
				} 
			}
			nlapiLogExecution('DEBUG', 'lowest is    ', lowestDifference);      
			if(lowIndex > -1){
				acctResult = acctResults[lowIndex];
				postingPeriod = acctResult.getId();
				//filters.push ( new nlobjSearchFilter('postingperiod', null, 'anyof', period) );
			} // end if		
			//	nlapiLogExecution('DEBUG', 'period is   is ', period); 
		}
		if(postingPeriod != null && postingPeriod != ''){
			filters.push ( new nlobjSearchFilter('postingperiod', null, 'anyof', postingPeriod) );
		}
		nlapiLogExecution('DEBUG', ' tipalti Record sync ', msg);
		// Define search columns
		var columns = new Array();
		columns[0] = new nlobjSearchColumn( 'entity' );  
		columns[1] = new nlobjSearchColumn( 'amountremaining' );
		columns[2] = new nlobjSearchColumn( 'tranid' );
		//	columns[3] = new nlobjSearchColumn( 'postingperiod' );
		columns[3] = new nlobjSearchColumn( 'saleseffectivedate' );		
		columns[4] = new nlobjSearchColumn('custbody_process_date_tipalti');
		columns[5] = new nlobjSearchColumn('type');
		// Execute the search. You must specify the internal ID of the record type.
		var searchresults = nlapiSearchRecord( 'transaction', null, filters, columns ); // go to dB find all records match. 1st type, 3rd critera, col= what info from record etc
		// Loop through all search results. When the results are returned, use methods
		// on the nlobjSearchResult object to get values for specific fields.
		for ( var i = 0; searchresults != null && i < searchresults.length; i++ ) // to get the info from records
		{
			//nlapiLogExecution('DEBUG', ' tipalti Record sync ', '  FOR LOOP  searchresults.length = ' +  searchresults.length );

			var searchresult = searchresults[ i ];
			var customer = searchresult.getValue( 'entity' );
			var projectedamount = searchresult.getValue( 'amountremaining' );
			var invoice = searchresult.getValue( 'tranid' );
			//var period = searchresult.getText( 'postingperiod' );
			var sales = searchresult.getValue( 'saleseffectivedate' );
			var date = searchresult.getValue ('trandate');	
			var createDate = searchresults.getValue ('datecreated');
			var invoiceId = searchresult.getId();	
			var dateFromInvoice = (searchresult.getValue('custbody_process_date_tipalti' ));
			var recType = searchresult.getRecordType();
			if(  dateFromInvoice == null || dateFromInvoice == '' )
			{
				try{
					var invoice = nlapiLoadRecord ( recType, invoiceId ); // loading the record
					if(invoice != null){			
						var curDate = new Date();
						var stringDate = nlapiDateToString(curDate, 'datetimetz');  // getting the system time.
						var timemsg ='  stringDate = '+ stringDate;
						//nlapiLogExecution('DEBUG', ' tipalti Record sync ', timemsg);
						invoice.setFieldValue('custbody_process_date_tipalti',stringDate );						
						var datemsg ='record Type = '+ recType +' curDate = '+ curDate+ ' stringDate = ' + stringDate;
						var update = nlapiSubmitRecord(invoice ); 					
						var errorField = nlapiLookupField(recType,invoiceId, 'custbody_taxerrorcode_tipalti' );
						if(errorField == null || errorField == ''){ // checking to see if the record was a sucess with the field update.
							// this is good		
							datemsg = ' update was a success! invoiceId = '+invoiceId + datemsg;
							nlapiLogExecution('DEBUG', ' tipalti Record sync ', datemsg);

						} 
						else{
							// this is bad	
							datemsg = 'there is an ERROR in the field custbody_taxerrorcode_tipalti  invoiceId = ' + invoiceId + ' customer = ' + customer + ' projectedamount = '+ projectedamount + ' invoice = '+ invoice ;// + ' rec type = ' + recType + ' ID = ' + nlapiGetRecordId() + ' exec context = ' + currentContext.getExecutionContext();
							nlapiLogExecution('ERROR', ' tipalti Record sync ', datemsg);
							errorCounter++;
						}			
					} // end if statement
				}
				catch(e)
				{
					msg = 'failed record date update' + e;
					nlapiLogExecution('ERROR',' tipalti Record sync  ', msg);
				}

			} // end if		
			if( currentContext.getRemainingUsage() < govLimit ) {
				var state = nlapiYieldScript();
				if( state.status == 'FAILURE')   {
					errmsg = "Failed to yield script, exiting: Reason = " + state.reason + " / Size = " + state.size;
					nlapiLogExecution('ERROR', ' tipalti Record sync  ', errmsg);
					return;
				} 
				else if ( state.status == 'RESUME' ){
					errmsg = "Resuming script because of " + state.reason + ".  Size = " + state.size;
					nlapiLogExecution('AUDIT', ' tipalti Record sync  ', errmsg);
				}
				// state.status will never be SUCCESS because a success would imply a yield has occurred.  The equivalent response would be yield

			}
			errmsg =  ' at end of for loop : stringDate = ' + 
			stringDate +' invoiceId = ' + invoiceId + ' customer = ' + customer + ' projectedamount = '+ projectedamount + ' invoice = '+ invoice;// + ' rec type = ' + recType + ' ID = ' + nlapiGetRecordId() + ' exec context = ' + currentContext.getExecutionContext();
			nlapiLogExecution('DEBUG', 'tipalti Record sync ', errmsg); //print
		} // end for
		if(searchresults != null ){
			msg = 'end of script:  there were this many errors in field custbody_taxerrorcode_tipaltiupdates ' + errorCounter + '. This is the amount of successful updates '  + (searchresults.length - errorCounter )+'.';
		}
		else{
			msg = 'could not find any transactions matching the parameters ' + 	paramMsg;
		}
		nlapiLogExecution('DEBUG', 'tipalti Record sync ', msg); //print

	}  // end 1st if
}

function runsyncProcess(){  // this is excuted when the button (Run Bath Process) is clicked on. 
	var post = new Array();
	alert(' tipalti sync Process has been submitted ');
	post['postingPeriod'] = nlapiGetFieldValue('custrecord_postingperiod_tipalti');
	post['endDate'] = nlapiGetFieldValue ('custrecord_enddate_tipalti');
	post['startDate'] = nlapiGetFieldValue('custrecord_startdate_tipalti');
	post['accountingPeriod'] = nlapiGetFieldValue('custrecord_accounting_period_tipalti');
	var resolve = nlapiResolveURL('SUITELET', 'customscript_syncsuitelet_tipalti', 'customdeploy_syncsuitelet_tipalti');    	// remeber to change this

	var request = nlapiRequestURL( resolve ,post);
}

