/**
 * Module Description
 * 
 * Version  Date            Author      Remarks
 * 1.00     10 Aug 2015     Mike		Initial Release
 * 1.01		17 Nov 2016		Mike		Added check to make sure that Vendor uses an allowable AP account for the update.	
 *
 */

/**
 * 
 * This function resets the error code from any previous sync attempts. It can only be done in client side, since the setfeldvalue does not work well
 * in the before load event.
 * 
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType 
 * 
 * @param {String} type Access mode: create, copy, edit
 * @returns {Void}
 */
function tipaltiEntityPageInit(type){
	nlapiSetFieldValue('custentity_wserrorcode_tipalti','');
}



/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @returns {Boolean} True to continue save, false to abort save
 */
function tipaltiEntitySaveRecord(){
	

	var apAccount = nlapiGetFieldValue('payablesaccount');
	var tipaltiInstallFlag = nlapiGetContext().getSetting('SCRIPT', 'custscript_installflag_tipalti');	
	var defaultAddress = nlapiGetFieldValue('defaultaddress');

	if(tipaltiInstallFlag == 'T') {
	//	alert('AP account = ' + apAccount + ' ' + nlapiLookupField('account', apAccount, 'custrecord_apacct_tipalti')) 
	//		+ ' address = ' + defaultAddress + '|' + nlapiGetFieldText('defaultaddress');
		if (apAccount != '' && apAccount != null && nlapiLookupField('account', apAccount, 'custrecord_apacct_tipalti') == 'T') {
			if (nlapiGetFieldValue('defaultaddress') == '' || nlapiGetFieldValue('defaultaddress') == null) {
				alert('You must provide a default billing address for Tipalti vendor integration');
				return false;
			}
		}
	}

    return true;
}

/**
 * 
 * This function validates the Vendor ID field. This is a custom field which is mapped to the external ID
 * on the customer record. It makes sure that this external ID is unique for the company. 
 * 
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @param {Number} linenum Optional line item number, starts from 1
 * @returns {Boolean} True to continue changing field value, false to abort value change
 */
function tipaltiEntityValidateField(type, name, linenum){
   
	// alert('Validate vendor entity Field = ' + type + name + linenum);
	if (name == 'companyname' || name == 'firstname' || name == 'lastname' || name == 'middlename') {
		var isPerson = nlapiGetFieldValue('isperson');
		var vendorName = '';
		
		if (isPerson == 'T') {
			// netsuite requires that at least firstname and lastname be populated on save. So only do look up when both fields have values.
			if (nlapiGetFieldValue('firstname') != '' && nlapiGetFieldValue('lastname') != '') {
				if (nlapiGetFieldValue('middlename') != '')
					vendorName = nlapiGetFieldValue('firstname') + ' ' +  nlapiGetFieldValue('middlename') + ' '+ nlapiGetFieldValue('lastname');
				else 
					vendorName = nlapiGetFieldValue('firstname') + ' ' +  nlapiGetFieldValue('lastname');				
			}
		} else
			vendorName = nlapiGetFieldValue('companyname');
	
		if (vendorName != '') {
			var curVendorId = nlapiGetRecordId();
			var filters = new Array();
			filters[0] = new nlobjSearchFilter( 'entityid', null, 'is', vendorName );
			var columns = new Array();
			columns[0] = new nlobjSearchColumn('entityid');
			var searchResults = nlapiSearchRecord( 'vendor', null, filters, columns);
			if (searchResults) {
				var srchVendorId = searchResults[0].getId();
				if (srchVendorId != curVendorId) {
					alert('This Tipalti Vendor ID is already in use, by ' + searchResults[0].getValue('entityid'));
					
					return false;
				}
			}
	
		}
	}

    return true;
}