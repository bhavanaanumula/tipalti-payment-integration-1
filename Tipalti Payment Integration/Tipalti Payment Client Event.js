/**
 * Module Description
 * 
 * Version    Date            Author            Remarks
 * 1.00       25 Oct 2015     Mike Canniff		Initial release.
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType 
 * 
 * @param {String} type Access mode: create, copy, edit
 * @returns {Void}
 */
function clientPageInit(type){
   
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @returns {Boolean} True to continue save, false to abort save
 */
function tipaltiClientSaveRecord(){
	var apAccount = nlapiGetFieldValue('apacct');
	var tipaltiInstallFlag = nlapiGetContext().getSetting('SCRIPT', 'custscript_installflag_tipalti');	

	if(tipaltiInstallFlag == 'T') {
	//	alert('AP account = ' + apAccount + ' ' + nlapiLookupField('account', apAccount, 'custrecord_apacct_tipalti'));
		if (nlapiLookupField('account', apAccount, 'custrecord_apacct_tipalti') == 'T') {
			var response = confirm('This payment is a manual payment not reflected in Tipalti. Do you want to continue?');
			if (!response)
				return false;
		}
	}

	
    return true;
}
